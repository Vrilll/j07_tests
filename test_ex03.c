/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex03.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/15 13:44:22 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/15 14:58:40 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>

char *ft_concat_params(int argc, char **argv);

int main(int argc, char **argv)
{
	char	*concat_str;

	concat_str = ft_concat_params(argc, argv);
	printf("concat_params: \n%s\n", concat_str);
	return (0);
}
