/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex04.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/15 18:37:19 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/15 21:38:49 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	**ft_split_whitespaces(char *str);

int		main(void)
{
	char **splitted;

	printf("test string: %s\n---\n", "123 234");
	splitted = ft_split_whitespaces("123 234");
	while (*splitted)
	{
		int ii = 0;
		printf("%p:\n", *splitted);
		while (ii < 5)
		{
			printf("0x%x ", (*splitted)[ii]);
			ii++;
		}
		printf("\n");
		splitted++;
	}
	return (0);
}
