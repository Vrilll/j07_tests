/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex02.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/14 16:59:06 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/15 13:37:40 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_ultimate_range(int **range, int min, int max);

int	main(void)
{
	int	*range;
	int	arr_size;
	int	arr_pos;


	arr_size = ft_ultimate_range(&range, -5, 5);
	printf("Testing ft_ultimate_range(ptr, -5, 5);\nsize: %d\n", arr_size);
	arr_pos = 0;
	while (arr_pos < arr_size)
	{
		printf("arr index [%d]: %d\n", arr_pos, range[arr_pos]);
		arr_pos++;
	}
}
