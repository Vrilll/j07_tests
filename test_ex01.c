/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex01.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/14 14:40:02 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/14 17:56:15 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stddef.h>

int	*ft_range(int min, int max);

int	main(void)
{
	int				*ft_array;
	unsigned int	arr_pos;

	printf("Testing ft_range(-5, 5);\n");
	ft_array = ft_range(-5, 5);
	arr_pos = 0;
	while (arr_pos <= 9)
	{
		printf("arr index [%d]: %d\n", arr_pos, *ft_array);
		ft_array++;
		arr_pos++;
	}
}
