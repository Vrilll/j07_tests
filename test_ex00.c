/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex00.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/14 14:17:34 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/14 14:22:33 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	*ft_strdup(char *src);

int		main(void)
{
	char	*orig_str;
	char	*ft_str;
	char	*std_str;

	orig_str = "Hello world!";
	ft_str = ft_strdup(orig_str);
	std_str = strdup(orig_str);
	printf("Testing str: %s\nft_strdup: %s\nstrdup: %s\ncompare: %d\n",
			orig_str,
			ft_str,
			std_str,
			strcmp(ft_str, std_str));
	return (0);
}
